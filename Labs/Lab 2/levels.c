#include "mraa.h"
#include <stdio.h>
#include <unistd.h>
#include"mraa/aio.h"
#include <math.h>


int level()
{

	// Declarations for the two LED lights
	mraa_gpio_context redLed = NULL;
	mraa_gpio_context yellowLed = NULL;

	redLed = mraa_gpio_init(7);
	yellowLed = mraa_gpio_init(8);

	// Declarations for the XAxix and YAxis
	mraa_aio_context xAxis, yAxis = NULL;

	xAxis = mraa_aio_init(0);
	yAxis = mraa_aio_init(1);

	//This sets the bits to 12
	//mraa_aio_set_bit(xAxis, 12);
	//mraa_aio_set_bit(yAxis, 12);
	// Declarations for the values of the Axis points
	int xAxisValue = 0;
	int yAxisValue = 0;

	//Need a value for the level
	float xLevel = 0;
	float yLevel = 0;

	// Determine what lights go on based on the values for the X and Y axis
	// Do this forever
	for(;;) {

		xAxisValue = mraa_aio_read(xAxis);
		yAxisValue = mraa_aio_read(yAxis);

		// Determine the levels here
		// 12 Bit
		//xLevel = (xAxisValue - 1449) / (551/2.0);
		//yLevel = (yAxisValue - 1437) /(568/2.0);

		//
		xLevel = (xAxisValue - 362) / (136/2.0);
		//usleep(250000);
		yLevel = (yAxisValue - 362) /(143/2.0);
		//usleep(250000);
		//printf( "x level avg: %f\n" , ((180/3.141592653589793) * asin(xLevel)));
		//printf( "y level avg : %f\n" , ((180/3.141592653589793) * asin(yLevel)) );

		// Logic
		if ( (fabsf((180/3.141592) * asin(xLevel)) > 10) || (fabsf((180/3.141592) * asin(yLevel)) > 10))
		//(  ( xLevel < (xLevel - 10)) || ( yLevel < (yLevel - 10)) || (xLevel > (xLevel + 10)) || ( yLevel > (yLevel + 10)))
		{
			// Turn off both LED lights
			mraa_gpio_write(redLed, 0);
			mraa_gpio_write(yellowLed, 0);
		}

		else if( (fabsf((180/3.141592) * asin(xLevel)) <= 10) && (fabsf((180/3.141592) * asin(yLevel)) <= 10))
		{
			// Turn on Yellow LED here
			mraa_gpio_write(redLed, 0);
			mraa_gpio_write(yellowLed, 1);
		}

		else if( (fabsf((180/3.141592) * asin(xLevel)) <= 1) && (fabsf((180/3.141592) * asin(yLevel)) <= 1))
		//( ((xLevel >= (xLevel - 1)) && (yLevel >= (yLevel - 1)) )||(( xLevel <= (xLevel + 1)) && (yLevel <= (yLevel + 1))) )
		{
			// Turn on both LED lights here
			mraa_gpio_write(redLed, 1);
			mraa_gpio_write(yellowLed, 1);
		}

		printf("Xaxis %f Yaxis  %f\n", xLevel, yLevel);

	}

}
