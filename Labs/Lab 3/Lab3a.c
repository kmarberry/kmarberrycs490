/*
 * Author: Jessica Gomez <jessica.gomez.hernandez@intel.com>
 * Copyright (c) 2015 Intel Corporation.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "mraa.h"

#include <stdio.h>
#include <unistd.h>

/*
 * On board LED blink C example
 *
 * Demonstrate how to blink the on board LED, writing a digital value to an
 * output pin using the MRAA library.
 * No external hardware is needed.
 *
 * - digital out: on board LED
 *
 * Additional linker flags: none
 */
	union{
		unsigned char DebBaIN;
		struct{
			unsigned char DebBaIN_0 :1; //button A
			unsigned char DebBaIN_1 :1; //button B
			unsigned char DebBaIN_2 :1; //button UP
			unsigned char DebBaIN_3 :1; //button DOWN
			unsigned char DebBaIN_4 :1; //button LEFT
			unsigned char DebBaIN_5 :1; //button RIGHT
			unsigned char DebBaIN_6 :1; //button CENTER
			unsigned char DebBaIN_7 :1; //spare bit
		}DebBaIN_bit;
	}DebBaIN;

#define DEBBA DebBaIN.DebBaIN_bit.DebBaIN_0
#define DEBBB DebBaIN.DebBaIN_bit.DebBaIN_1
//#define RAWBA 0x08
#define PRESS_THRESHOLD 0x3F
#define RELEASE_THRESHOLD 0xFC

int nonInterupts()
{

	mraa_init();
	mraa_gpio_context buttonA = mraa_gpio_init_raw(49);
	mraa_gpio_context buttonB = mraa_gpio_init_raw(46);
	mraa_gpio_mode(buttonA,1);
	mraa_gpio_mode(buttonB,1);

	DebBaIN.DebBaIN = 0xFF;

	static uint8_t BAShiftReg = 0xFF;
	static uint8_t BBShiftReg = 0xFF;

	// loop forever toggling the on board LED every second
	for (;;) {

		BAShiftReg >>=1;
		int BA_out = mraa_gpio_read(buttonA);

		if(BA_out == 1)
		{
			BAShiftReg |= 0x80;
		}

		if(DEBBA == 0 )
		{
			if(BAShiftReg == RELEASE_THRESHOLD)
			{
				printf("button A : Released. \n");
				DEBBA = 1;
			}
		}
		else if (DEBBA == 1)
		{
			if(BAShiftReg <= PRESS_THRESHOLD)
			{
				printf("button A : pressed. \n");
				DEBBA = 0;
			}
		}

		int BB_out = mraa_gpio_read(buttonB);

		BBShiftReg >>=1;

		if (BB_out == 1)
		{
			BBShiftReg |= 0x80;
		}

		if(DEBBB == 0 )
			{
				if (BBShiftReg == RELEASE_THRESHOLD)
				{
					printf("button B : Released. \n");
					DEBBB = 1;
				}
			}
			else
			{
				if(BBShiftReg <= PRESS_THRESHOLD)
				{
					printf("button B : pressed. \n");
					DEBBB = 0;
				}
			}
		usleep(5000);
		//mraa_gpio_write(d_pin, 1);
		//sleep(1);
	}

	return MRAA_SUCCESS;
}

//void main ()
//{
//	nonInterupts();
//
//
//}
