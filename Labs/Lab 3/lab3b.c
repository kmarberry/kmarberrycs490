/*
 * lab3b.c
 *
 *  Created on: Oct 21, 2015
 *      Author: root
 */

#include <unistd.h>
#include <stdio.h>
#include "mraa.h"


#define PRESS_THRESHOLD 0x3F
#define PRESS_RELEASE 0xFC
static uint8_t buttonA = 1;
static mraa_gpio_context bA = NULL;
static uint8_t buttonB = 1;
static mraa_gpio_context bB = NULL;
static uint8_t buttonUp = 1;
static mraa_gpio_context bU = NULL;
static uint8_t buttonDown = 1;
static mraa_gpio_context bD = NULL;
static uint8_t buttonLeft = 1;
static mraa_gpio_context bL = NULL;
static uint8_t buttonRight = 1;
static mraa_gpio_context bR = NULL;
static uint8_t buttonCenter = 1;
static mraa_gpio_context bC = NULL;

void buttonA_Itr(void * arg)
{
	int read = 0;
		for (;;) {
				usleep(5000);
				static uint8_t shiftRegister = 0xFF;
				shiftRegister >>= 1;
				read = mraa_gpio_read(bA);

				// If not pressed
				if (read == 1) {

					shiftRegister |= (1 << 7);
				}

				// If pressed
				if (buttonA == 0) {
					if (shiftRegister >= PRESS_RELEASE) {
						buttonA = 1;
						printf("button A is released\n");
					}

				} else {
					if (shiftRegister <= PRESS_THRESHOLD) {
						buttonA = 0;
						printf("button A is pressed\n");
					}
				}

			}
}
void buttonB_Itr(void * arg)
{
	int read = 0;
			for (;;) {
					usleep(5000);
					static uint8_t shiftRegister = 0xFF;
					shiftRegister >>= 1;
					read = mraa_gpio_read(bB);

					// If not pressed
					if (read == 1) {

						shiftRegister |= (1 << 7);
					}

					// If pressed
					if (buttonB == 0) {
						if (shiftRegister >= PRESS_RELEASE) {
							buttonB = 1;
							printf("button B is released\n");
						}

					} else {
						if (shiftRegister <= PRESS_THRESHOLD) {
							buttonB = 0;
							printf("button B is pressed\n");
						}
					}

				}
}
void buttonUP_Itr(void * arg)
{
	int read = 0;
			for (;;) {
					usleep(5000);
					static uint8_t shiftRegister = 0xFF;
					shiftRegister >>= 1;
					read = mraa_gpio_read(bU);

					// If not pressed
					if (read == 1) {

						shiftRegister |= (1 << 7);
					}

					// If pressed
					if (buttonUp == 0) {
						if (shiftRegister >= PRESS_RELEASE) {
							buttonUp = 1;
							printf("button up is released\n");
						}

					} else {
						if (shiftRegister <= PRESS_THRESHOLD) {
							buttonUp = 0;
							printf("button up is pressed\n");
						}
					}

				}
}
void buttonDOWN_Itr(void * arg)
{
	int read = 0;
			for (;;) {
					usleep(5000);
					static uint8_t shiftRegister = 0xFF;
					shiftRegister >>= 1;
					read = mraa_gpio_read(bD);

					// If not pressed
					if (read == 1) {

						shiftRegister |= (1 << 7);
					}

					// If pressed
					if (buttonDown == 0) {
						if (shiftRegister >= PRESS_RELEASE) {
							buttonDown = 1;
							printf("button down is released\n");
						}

					} else {
						if (shiftRegister <= PRESS_THRESHOLD) {
							buttonDown = 0;
							printf("button down is pressed\n");
						}
					}

				}
}
void buttonLEFT_Itr(void * arg)
{
	int read = 0;
			for (;;) {
					usleep(5000);
					static uint8_t shiftRegister = 0xFF;
					shiftRegister >>= 1;
					read = mraa_gpio_read(bL);

					// If not pressed
					if (read == 1) {

						shiftRegister |= (1 << 7);
					}

					// If pressed
					if (buttonLeft == 0) {
						if (shiftRegister >= PRESS_RELEASE) {
							buttonLeft = 1;
							printf("button left is released\n");
						}

					} else {
						if (shiftRegister <= PRESS_THRESHOLD) {
							buttonLeft = 0;
							printf("button left is pressed\n");
						}
					}

				}
}
void buttonRIGHT_Itr(void * arg)
{
	int read = 0;
			for (;;) {
					usleep(5000);
					static uint8_t shiftRegister = 0xFF;
					shiftRegister >>= 1;
					read = mraa_gpio_read(bR);

					// If not pressed
					if (read == 1) {

						shiftRegister |= (1 << 7);
					}

					// If pressed
					if (buttonRight == 0) {
						if (shiftRegister >= PRESS_RELEASE) {
							buttonRight = 1;
							printf("button right is released\n");
						}

					} else {
						if (shiftRegister <= PRESS_THRESHOLD) {
							buttonRight = 0;
							printf("button right is pressed\n");
						}
					}

				}
}
void buttonCENTER_Itr(void * arg)
{
	int read = 0;
			for (;;) {
					usleep(5000);
					static uint8_t shiftRegister = 0xFF;
					shiftRegister >>= 1;
					read = mraa_gpio_read(bC);

					// If not pressed
					if (read == 1) {

						shiftRegister |= (1 << 7);
					}

					// If pressed
					if (buttonCenter == 0) {
						if (shiftRegister >= PRESS_RELEASE) {
							buttonCenter = 1;
							printf("button center is released\n");
						}

					} else {
						if (shiftRegister <= PRESS_THRESHOLD) {
							buttonCenter = 0;
							printf("button center is pressed\n");
						}
					}

				}
}



//
//void interrupt(void* args) {
//
//
//
//
//}

int main() {
	//static mraa_gpio_context buttonUp = NULL;
	mraa_init();
	bA = mraa_gpio_init_raw(49);
	mraa_gpio_dir(bA, MRAA_GPIO_IN);

	bB = mraa_gpio_init_raw(46);
	mraa_gpio_dir(bB, MRAA_GPIO_IN);

	bU = mraa_gpio_init_raw(47);
	mraa_gpio_dir(bU, MRAA_GPIO_IN);

	bD = mraa_gpio_init_raw(44);
	mraa_gpio_dir(bD, MRAA_GPIO_IN);

	bL = mraa_gpio_init_raw(165);
	mraa_gpio_dir(bL, MRAA_GPIO_IN);

	bR = mraa_gpio_init_raw(45);
	mraa_gpio_dir(bR, MRAA_GPIO_IN);

	bC = mraa_gpio_init_raw(48);
	mraa_gpio_dir(bC, MRAA_GPIO_IN);

	mraa_gpio_edge_t edge = MRAA_GPIO_EDGE_BOTH;

	mraa_gpio_isr(bA, edge, &buttonA_Itr, NULL);
	mraa_gpio_isr(bB, edge, &buttonB_Itr, NULL);
	mraa_gpio_isr(bU, edge, &buttonUP_Itr, NULL);
	mraa_gpio_isr(bD, edge, &buttonDOWN_Itr, NULL);
	mraa_gpio_isr(bL, edge, &buttonLEFT_Itr, NULL);
	mraa_gpio_isr(bR, edge, &buttonRIGHT_Itr, NULL);
	mraa_gpio_isr(bC, edge, &buttonCENTER_Itr, NULL);


	for (;;) {


	}

	mraa_gpio_close(bA);
	mraa_gpio_close(bB);
	mraa_gpio_close(bU);
	mraa_gpio_close(bD);
	mraa_gpio_close(bL);
	mraa_gpio_close(bR);
	mraa_gpio_close(bC);

	return MRAA_SUCCESS;
}


