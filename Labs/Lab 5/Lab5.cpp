#include <iostream>
#include "mraa.h"

// Declare the pins
// Motor 1
#define STEP 7
#define DIR 6
#define ENABLE 4

// Motor 2
#define STP 10
#define DR 9
#define EN 8
//

// Laser Vmod (digital input to laser controller)
#define vmod 3 // laser-controller depth of field maybe?

// Laser Power (MOSFET gate)
#define power 2 // gate

// LED Light Power (MOSFET gate)
#define ledPower 5

// Function Prototypes
// Part 1 Functions
void moveForward();
void moveBackward();
void laser();




int main ()
{
	//moveForward();
	laser();
}


void moveForward() {


	// Initialize Pins
	mraa_gpio_context enablePin = NULL;
	mraa_gpio_context directionPin = NULL;
	mraa_gpio_context stepPin = NULL;


	// Initialize Motor 2  Pins
	mraa_gpio_context ePin = NULL;
	mraa_gpio_context dPin = NULL;
				mraa_gpio_context sPin = NULL;

	// Enable Motor 1 Pins
	enablePin = mraa_gpio_init(ENABLE);
	directionPin = mraa_gpio_init(DIR);
	stepPin = mraa_gpio_init(STEP);

	// Enable Motor 2 Pinst
	ePin = mraa_gpio_init(EN);
	dPin = mraa_gpio_init(DR);
	sPin = mraa_gpio_init(STP);

	//Outputs
	mraa_gpio_dir(enablePin, MRAA_GPIO_OUT );
	mraa_gpio_dir(directionPin, MRAA_GPIO_OUT);
	mraa_gpio_dir(stepPin, MRAA_GPIO_OUT);


	//Outputs motor 2
	mraa_gpio_dir(ePin, MRAA_GPIO_OUT );
	mraa_gpio_dir(dPin, MRAA_GPIO_OUT);
	mraa_gpio_dir(sPin, MRAA_GPIO_OUT);


	// Write the pins
	mraa_gpio_write(enablePin, 0);
	//printf("Outside Loop");
	mraa_gpio_write(directionPin, 0);
	int degrees = 10;
	int stepCount = (int)(degrees / 0.9) * 16;

	int i;
	for(i =0; i < stepCount; i++)
	{
		mraa_gpio_write(stepPin, 0);
		usleep(2000);
		mraa_gpio_write(stepPin, 1);
		usleep(2000);

		// Move motor 2
		mraa_gpio_write(sPin, 0);
		usleep(2000);
		mraa_gpio_write(sPin, 1);
		usleep(2000);
	}

}

void led()
{
	// Initialize Pins
	mraa_pwm_context LED  = NULL;
	LED = mraa_pwm_init(ledPower);

	mraa_pwm_period_us(LED, 10);
	mraa_pwm_write(LED, 0.01);
	mraa_pwm_enable (LED, 1);
	sleep(2);
	mraa_pwm_enable (LED, 0);
	// Turn both the laser on and off
}
void laser(){
	// Initialize Pins
	mraa_pwm_context laser  = NULL;
	mraa_gpio_context LP  = NULL;
	LP = mraa_gpio_init(power);
	mraa_gpio_dir(LP, MRAA_GPIO_OUT );
	mraa_gpio_write(LP,1);
	//
	laser = mraa_pwm_init(vmod);
	mraa_pwm_period_us(laser, 10);
	mraa_pwm_write(laser, 0.5);
	mraa_pwm_enable (laser, 1);
	sleep(5);
	mraa_pwm_enable (laser, 0);
	mraa_gpio_write(LP,0);


}












