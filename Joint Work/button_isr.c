#include <unistd.h>
#include <stdio.h>
#include "mraa.h"


#define PRESS_THRESHOLD 0x3F
#define PRESS_RELEASE 0xFC
static uint8_t buttonA = 1;
static mraa_gpio_context buttonUp = NULL;

void interrupt(void* args) {

	int read = 0;
	for (;;) {
			usleep(5000);
			static uint8_t shiftRegister = 0xFF;
			shiftRegister >>= 1;
			read = mraa_gpio_read(buttonUp);

			// If not pressed
			if (read == 1) {

				shiftRegister |= (1 << 7);
			}

			// If pressed
			if (buttonA == 0) {
				if (shiftRegister >= PRESS_RELEASE) {
					buttonA = 1;
					printf("button up is released\n");
				}

			} else {
				if (shiftRegister <= PRESS_THRESHOLD) {
					buttonA = 0;
					printf("button up is pressed\n");
				}
			}

		}



}

int main() {
	//static mraa_gpio_context buttonUp = NULL;

	buttonUp = mraa_gpio_init_raw(47);
	mraa_gpio_dir(buttonUp, MRAA_GPIO_IN);
	if (buttonUp == NULL) {
		return 1;
	}

	//mraa_gpio_dir(buttonUp, MRAA_GPIO_IN);

	mraa_gpio_edge_t edge = MRAA_GPIO_EDGE_BOTH;

	mraa_gpio_isr(buttonUp, edge, &interrupt, NULL);

	for (;;) {

		// got to relieve our poor CPU!
//		usleep(5000);
	//	fprintf(stdout, "%d\n", shiftRegister);
	}

	mraa_gpio_close(buttonUp);

	return MRAA_SUCCESS;
}


