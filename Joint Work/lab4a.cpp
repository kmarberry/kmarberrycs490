#include <iostream>
#include "mraa.hpp"
#include <ssd1327.h>
#include <ssd1308.h>
#include <sainsmartks.h>
#include <lcm1602.h>
#include <jhd1313m1.h>
#include <eboled.h>
#include "oled/Edison_OLED.h"

using namespace std;
/////////////////////
// Tempature ////////
/////////////////////
#define XM_ADDR 		0x1D // used for Temp & Accel/Mag
#define CTRL_REG5_XM 	0x24 // temp pin
#define OUT_TEMP_H_XM 	0x06
#define OUT_TEMP_L_XM 	0x05
//*******************
/////////////////////
// Accel & Gyro//////
/////////////////////
#define G_ADDR 			0x6B
#define CTRL_REG5_G		0x24 // Gyro pin
#define OUT_X_L_A		0x28
#define OUT_X_H_A		0x29
#define OUT_Y_L_A		0x2A
#define OUT_Y_H_A		0x2B
#define OUT_Z_L_A		0x2C
#define OUT_Z_H_A		0x2D
//*******************
/////////////////////
///Mag///////////////
/////////////////////
#define CTRL_REG6_XM	0x25 // magnetometer
#define CTRL_REG1_XM	0x20 // Accelerometer
#define OUT_X_L_M		0x08
#define OUT_X_H_M		0x09
#define OUT_Y_L_M		0x0A
#define OUT_Y_H_M		0x0B
#define OUT_Z_L_M		0x0C
#define OUT_Z_H_M		0x0D
//*******************
/////////////////////
//buttons////////////
/////////////////////
#define PRESS_THRESHOLD 0x3F
#define PRESS_RELEASE 0xFC
static uint8_t buttonA = 1;
static mraa_gpio_context buttonUp = NULL;
//*******************
int16_t assemble(uint8_t, uint8_t);
int16_t tempAssemble(uint8_t, uint8_t);
void readTemp();
void gyro();
void accel();
void mag();
void interrupt(void* args);
//*******************
// Define an edOLED object:
edOLED oled;
double tempature = 0.0;
int counter = 1;

int main(){

	buttonUp = mraa_gpio_init_raw(47);
	mraa_gpio_dir(buttonUp, MRAA_GPIO_IN);
	if (buttonUp == NULL) {
		return 1;
	}

	mraa_gpio_edge_t edge = MRAA_GPIO_EDGE_BOTH;

	mraa_gpio_isr(buttonUp, edge, &interrupt, NULL);
	oled.begin();
	for(;;)
	{
		switch(counter){
		case 1:

			oled.clear(PAGE);
			oled.setCursor(0,0);
			oled.print("Press up to move through the pages");
			oled.display();
			break;
		case 2:
			readTemp();
			break;
		case 3:
			accel();
			break;
		case 4:
			gyro();
			break;
		case 5:
			mag();
			break;
		}
		//readTemp();
		//gyro();
		//accel();
		//mag();
		usleep(20000);
	};

	mraa_gpio_close(buttonUp);
	return MRAA_SUCCESS;;
}

int16_t assemble(uint8_t low, uint8_t hi){
	uint16_t l = (uint16_t) low;
	uint16_t h = (uint16_t) hi;

	return (int16_t)((h << 8) | l);
}

int16_t tempAssemble(uint8_t low, uint8_t hi){
	int16_t temp;
	if(hi >= 0x08){
		temp = 0xF0;
		temp |= hi;
	}
	else{
		temp = 0x00;
		temp ^= hi;
	}
	return assemble(low, temp);
}

void readTemp(){
	oled.clear(PAGE);
	oled.setCursor(0,0);

	mraa::I2c* i2c = new mraa::I2c(1);
	i2c->address(XM_ADDR);

	i2c -> writeReg(CTRL_REG5_XM, 0x98);

	uint8_t lowByte; // i2c -> readReg(OUT_TEMP_H_XM);
	uint8_t hiByte; //i2c -> readReg(OUT_TEMP_L_XM);

	int16_t lowHiByte;


	lowByte = i2c -> readReg(OUT_TEMP_L_XM);
	hiByte = i2c -> readReg(OUT_TEMP_H_XM);
	lowHiByte = tempAssemble(lowByte, hiByte);

	double lowDiv = lowHiByte / 8.0;
	tempature = 21 + lowDiv;
	char* temp = new char [100];
	sprintf(temp, "%.3f", tempature);
	oled.print(temp);
	printf("%f \n", tempature);
	oled.display();


}

void gyro(){
	oled.clear(PAGE);
	oled.setCursor(0,0);
	mraa::I2c* i2c = new mraa::I2c(1);
	i2c->address(G_ADDR);

	i2c -> writeReg(CTRL_REG5_G, 0x00);

	uint8_t xLow; // i2c -> readReg(OUT_X_L_G);
	uint8_t xHigh;  //i2c -> readReg(OUT_X_H_G);
	uint8_t yLow; // i2c -> readReg(OUT_Y_L_G);
	uint8_t yHigh;  //i2c -> readReg(OUT_Y_H_G);
	uint8_t zLow; // i2c -> readReg(OUT_Z_L_G);
	uint8_t zHigh;  //i2c -> readReg(OUT_Z_H_G);
	//__________________________________________________
	int16_t x16;
	int16_t y16;
	int16_t z16;
	//__________________________________________________
	xLow = i2c -> readReg(OUT_X_L_M);
	xHigh = i2c -> readReg(OUT_X_H_M);
	yLow = i2c -> readReg(OUT_Y_L_M);
	yHigh = i2c -> readReg(OUT_Y_H_M);
	zLow = i2c -> readReg(OUT_Z_L_M);
	zHigh = i2c -> readReg(OUT_Z_H_M);
	//__________________________________________________
	x16 = assemble(xLow, xHigh);
	y16 = assemble(yLow, yHigh);
	z16 = assemble(zLow, zHigh);
	//__________________________________________________
	oled.print("X= ");		oled.print(x16);
	oled.print("\nY= ");	oled.print(y16);
	oled.print("\nZ= ");	oled.print(z16);
	oled.display();

}

void accel(){
	oled.clear(PAGE);
	oled.setCursor(0,0);
	//	printf("x value print please");
	mraa::I2c* i2c = new mraa::I2c(1);
	i2c->address(XM_ADDR);
	i2c -> writeReg(CTRL_REG1_XM, 0x57);

	uint8_t xLow; 	// i2c -> readReg(OUT_X_L_M);
	uint8_t xHigh;  // i2c -> readReg(OUT_X_H_M);
	uint8_t yLow; 	// i2c -> readReg(OUT_Y_L_M);
	uint8_t yHigh;  // i2c -> readReg(OUT_Y_H_M);
	uint8_t zLow; 	// i2c -> readReg(OUT_Z_L_M);
	uint8_t zHigh;  // i2c -> readReg(OUT_Z_H_M);
	//__________________________________________________
	int16_t x16;
	int16_t y16;
	int16_t z16;
	//__________________________________________________
	xLow = i2c -> readReg(OUT_X_L_A);
	xHigh = i2c -> readReg(OUT_X_H_A);
	yLow = i2c -> readReg(OUT_Y_L_A);
	yHigh = i2c -> readReg(OUT_Y_H_A);
	zLow = i2c -> readReg(OUT_Z_L_A);
	zHigh = i2c -> readReg(OUT_Z_H_A);
	//__________________________________________________
	x16 = assemble(xLow, xHigh);
	y16 = assemble(yLow, yHigh);
	z16 = assemble(zLow, zHigh);
	//__________________________________________________
//	x16 = assemble(xLow, xHigh);
//	y16 = assemble(yLow, yHigh);
//	z16 = assemble(zLow, zHigh);
	//__________________________________________________
	oled.print("X= ");		oled.print(x16);
//	printf("x is %d\n");
	oled.print("\nY= ");	oled.print(y16);
	oled.print("\nZ= ");	oled.print(z16);
	oled.display();
}

void mag(){
	oled.clear(PAGE);
	oled.setCursor(0,0);

	//	printf("x value print please");
	mraa::I2c* i2c = new mraa::I2c(1);
	i2c->address(XM_ADDR);
	i2c -> writeReg(CTRL_REG6_XM, 0x00);

	uint8_t xLow; 	// i2c -> readReg(OUT_X_L_M);
	uint8_t xHigh;  // i2c -> readReg(OUT_X_H_M);
	uint8_t yLow; 	// i2c -> readReg(OUT_Y_L_M);
	uint8_t yHigh;  // i2c -> readReg(OUT_Y_H_M);
	uint8_t zLow; 	// i2c -> readReg(OUT_Z_L_M);
	uint8_t zHigh;  // i2c -> readReg(OUT_Z_H_M);
	//__________________________________________________
	int16_t x16;
	int16_t y16;
	int16_t z16;
	//__________________________________________________
	xLow = i2c -> readReg(OUT_X_L_A);
	xHigh = i2c -> readReg(OUT_X_H_A);
	yLow = i2c -> readReg(OUT_Y_L_A);
	yHigh = i2c -> readReg(OUT_Y_H_A);
	zLow = i2c -> readReg(OUT_Z_L_A);
	zHigh = i2c -> readReg(OUT_Z_H_A);
	//__________________________________________________
	x16 = assemble(xLow, xHigh);
	y16 = assemble(yLow, yHigh);
	z16 = assemble(zLow, zHigh);
	//__________________________________________________

	oled.print("X= ");		oled.print(x16);
	oled.print("\nY= ");	oled.print(y16);
	oled.print("\nZ= ");	oled.print(z16);
	oled.display();
}

void interrupt(void* args) {

	int read = 0;
	for (;;) {
		usleep(5000);
		static uint8_t shiftRegister = 0xFF;
		shiftRegister >>= 1;
		read = mraa_gpio_read(buttonUp);

		// If not pressed
		if (read == 1) {

			shiftRegister |= (1 << 7);
		}

		// If pressed
		if (buttonA == 0) {
			if (shiftRegister >= PRESS_RELEASE) {
				buttonA = 1;
				printf("button up is released\n");
			}

		} else {
			if (shiftRegister <= PRESS_THRESHOLD) {
				buttonA = 0;
				counter++;
				if(counter >= 6){
					counter = 1;
				}
				printf("button up is pressed\n");
			}
		}

	}
}

