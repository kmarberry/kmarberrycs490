

#include "mraa.h"
#include <stdio.h>
#include <unistd.h>
//#include"mraa/aio.h"

int blinkLED()
{

	mraa_platform_t platform = mraa_get_platform_type();
	mraa_gpio_context d_pin = NULL;
	mraa_gpio_context e_pin = NULL;
	switch (platform) {
	case MRAA_INTEL_GALILEO_GEN2:
		d_pin = mraa_gpio_init(7);
		e_pin = mraa_gpio_init(8);
		break ;
	case MRAA_INTEL_EDISON_FAB_C:
		d_pin = mraa_gpio_init(7);
		e_pin = mraa_gpio_init(8);
		break;
	default:
		fprintf(stderr, "Unsupported platform, exiting");
		return MRAA_ERROR_INVALID_PLATFORM;
	}
	if (d_pin == NULL) {
		fprintf(stderr, "MRAA couldn't initialize GPIO, exiting");
		return MRAA_ERROR_UNSPECIFIED;
	}

	// set the pin as output
	if (mraa_gpio_dir(d_pin, MRAA_GPIO_OUT) != MRAA_SUCCESS) {
		fprintf(stderr, "Can't set digital pin as output, exiting");
		return MRAA_ERROR_UNSPECIFIED;
	};

	// loop forever toggling the on board LED every second
	for (;;) {
		mraa_gpio_write(d_pin, 0);
		sleep(1);
		mraa_gpio_write(e_pin, 1);
		sleep(1);
		mraa_gpio_write(d_pin, 1);
		sleep(1);
		mraa_gpio_write(e_pin, 0);
		sleep(1);
	}

	return MRAA_SUCCESS;
}
int accelerometer(){

	mraa_aio_context xAxis, yAxis;
	unsigned int xValue, yValue;
	float xFloat = 0.0, yFloat = 0.0;


	xAxis = mraa_aio_init(0);
	yAxis = mraa_aio_init(1);


	if (xAxis  == NULL || yAxis == NULL) {
		return 1;
	}


	int n = 5, count; // count for inner loop
	for (;;) {
		for(count = 0; count < n; ++count){
			xValue = mraa_aio_read(xAxis);
			yValue = mraa_aio_read(yAxis);
			xFloat = mraa_aio_read_float(xAxis);
			yFloat = mraa_aio_read_float(yAxis);

			printf("Xaxis %d %x %f\n", xValue, xValue, xFloat);
			printf("Yaxis %d %x %f\n", yValue, yValue, yFloat);
			usleep(20000);
		}

		printf("\n");
		sleep(1);
	}

	return MRAA_SUCCESS;
}

int main()
{
		//blinkLED();
	accelerometer();

	return MRAA_SUCCESS;

}

