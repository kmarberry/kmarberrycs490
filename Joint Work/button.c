#include "mraa.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>



uint8_t buttonA = 1;


#define PRESS_THRESHOLD 0x3F
#define PRESS_RELEASE 0xFC

void debounce ()
{


	mraa_gpio_context buttonUp = NULL;
	buttonUp = mraa_gpio_init_raw(47);
	mraa_gpio_dir(buttonUp, MRAA_GPIO_IN);

	int read = 0;
	for (;;) {
		usleep(5000);
		static uint8_t shiftRegister = 0xFF;
		shiftRegister >>= 1;
		read = mraa_gpio_read(buttonUp);

		// If not pressed
		if (read == 1) {

			shiftRegister |= (1 << 7);
		}

		// If pressed
		if (buttonA == 0) {
			if (shiftRegister >= PRESS_RELEASE) {
				buttonA = 1;
				printf("button up is released\n");
			}

		} else {
			if (shiftRegister <= PRESS_THRESHOLD) {
				buttonA = 0;
				printf("button up is pressed\n");
			}
		}

	}


}


int main ()
{
	debounce();
}
