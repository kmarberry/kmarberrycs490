#include <iostream>
#include "mraa.hpp"
#include <ssd1327.h>
#include <ssd1308.h>
#include <sainsmartks.h>
#include <lcm1602.h>
#include <jhd1313m1.h>
#include <eboled.h>
#include "oled/Edison_OLED.h"
#include <math.h>



using namespace std;

#define XM_ADDR 0x1D
#define CRTL_REG5_XM 0x24 // tempature pin
#define OUT_TEMP_H_XM 0x06 // high pin
#define OUT_TEMP_L_XM 0x05 // low pin


//___________________________________________________________________________________
int16_t assemble(uint8_t, uint8_t);
int16_t tempAssemble(uint8_t, uint8_t);
void setupOLED();

//___________________________________________________________________________________
// Define an edOLED object:
edOLED oled;
double tempature = 0.0;
double tempValue;
int main(){
	setupOLED();


	mraa::I2c* i2c = new mraa::I2c(1);
	i2c->address(XM_ADDR);

	i2c -> writeReg(CRTL_REG5_XM, 0x98);

	uint8_t lowByte; // i2c -> readReg(OUT_TEMP_H_XM);
	uint8_t hiByte; //i2c -> readReg(OUT_TEMP_L_XM);

	int16_t lowHiByte;

	for(;;){
		lowByte = i2c -> readReg(OUT_TEMP_L_XM);

		hiByte = i2c -> readReg(OUT_TEMP_H_XM);

		lowHiByte = tempAssemble(lowByte, hiByte);

		double lowDiv = lowHiByte / 8.0;
		tempature = 21 + lowDiv;
		int temps = tempature;
		tempValue = (tempature - temps);
		int dec = tempValue * 100.0;
		oled.print(tempature);
		oled.print(".");
		oled.print(dec);
		oled.display();


		cout << tempature << endl;
		usleep(200000);
		setupOLED();
	}


	return 0;
}

int16_t assemble(uint8_t low, uint8_t hi){
	uint16_t l = (uint16_t) low;
	uint16_t h = (uint16_t) hi;

	return (int16_t)((h << 8) | l);
}

int16_t tempAssemble(uint8_t low, uint8_t hi){
	int16_t temp;
	if(hi >= 0x08){
		temp = 0xF0;
		temp |= hi;
	}
	else{
		temp = 0x00;
		temp ^= hi;
	}
	return assemble(low, temp);
}

void setupOLED()
{
	oled.begin();
	oled.clear(PAGE);
	oled.display();
	oled.setFontType(0);
}





